$(function () {
    updateTable()
});

function updateTable() {
    $.ajax({
        type: 'get',
        dataType: 'json',
        url: '/api/table-data.php',
        success: function (data) {
            let tableRows = ""
            data.forEach(el => {
                tableRows += `\
                <tr>
                    <td id="gdnName" class="td">${el.gardenName}</td>
                    <td id="gdnAge" class="td">${el.gardenAge}</td>
                    <td id="treeAmount" class="td">${el.treesInGarden}</td>
                    <td id="appleAmount" class="td">${el.applesInGarden}</td>
                    <td id="fallenApples" class="td">${el.fallenApples}</td>
                    <td id="rottenApples" class="td">${el.rottenApples}</td>
                </tr> `
            })
            $('#gardens-table tbody').html(tableRows);
        }
    });
}