$(function () {
    $("#gardenDaySkipForm").on("submit", function (event) {
        event.preventDefault();

        var gardenData = $(this).serialize();

        $.ajax({
            type: 'post',
            url: '/api/day-skip.php',
            data: gardenData,
            success: function (data) {
                updateTable()
            }
        });
    })
})
