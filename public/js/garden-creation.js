$(function () {
    $("#gardenCreationForm").on("submit", function (event) {
        let gardenData = $(this).serialize();
        $.ajax({
            type: 'post',
            url: '/api/garden-creation.php',
            data: gardenData,
            success: function (data) {
                alert(data)
                updateTable();
            },
            complete: function (resp) {
                if (resp.status > 299) {
                    alert(resp.responseText);
                }
            }
        });
        return false;
    });
});