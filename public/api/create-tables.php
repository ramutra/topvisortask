<?php

require_once "../../bootstrap.php";
require_once "../../App/Database/migrations/tables-creation.php";

use App\Database\Connection;

$conn = Connection::getConnection();

tableGarden($conn);
tableAppleTrees($conn);
tableApples($conn);