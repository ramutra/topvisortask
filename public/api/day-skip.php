<?php

require_once "../../bootstrap.php";

use App\Database\Connection;

use App\Models\Apple;
use App\Repositories\GardenRepository;
use App\Repositories\TreeRepository;
use App\Repositories\AppleRepository;

if ($_SERVER['REQUEST_METHOD'] === 'POST') {
    $gardenName = $_POST['gardenName'];

    $conn = Connection::getConnection();
    $gardenRepo = new GardenRepository($conn);
    $treeRepo = new TreeRepository($conn);
    $appleRepo = new AppleRepository($conn);

    try {
        $conn->beginTransaction();

        $gardenRepo->changeGardenAge($gardenName);
        $updatedGardenInfo = $gardenRepo->selectGardenByName($gardenName);

        $treesData = $treeRepo->selectTreesByGardenId((int)$updatedGardenInfo['id']);

        $updatedTreesAge = [];
        $updatedApplesAge = [];

        $agedApplesReadyToChangeLocationIds = [];
        $agedApplesReadyToChangeRottennessIds = [];

        foreach ($treesData as $tree) {
            $updatedTreesAge[$tree['id']] = (int)$updatedGardenInfo['age'] - $tree['garden_age_on_tree_creation'];
            $applesData = $appleRepo->selectApplesByTreeId((int)$tree['id']);

            foreach ($applesData as $apple) {
                $updatedApplesAge[$apple['id']] = $updatedTreesAge[$tree['id']] - $apple['garden_age_on_apple_creation'];

                if ((int)$updatedApplesAge[$apple['id']] === Apple::APPLE_FALL_FROM_TREE_ON_DAY) {
                    $appleRepo->UpdateAppleLocation((int)$apple['id']);
                }

                if ((int)$updatedApplesAge[$apple['id']] === Apple::APPLE_BECOME_ROTTEN_ON_DAY) {
                    $appleRepo->UpdateAppleRottenness((int)$apple['id']);
                }
            }
        }

        $conn->commit();
    } catch (Exception $e) {
        $conn->rollBack();

        echo $e->getMessage();
    }
}