<?php
require_once "../../bootstrap.php";
ini_set('display_errors', 1);

use App\Database\Connection;

use App\Models\Apple;
use App\Repositories\GardenRepository;
use App\Repositories\TreeRepository;
use App\Repositories\AppleRepository;

$conn = Connection::getConnection();

$gardenRepo = new GardenRepository($conn);
$treeRepo = new TreeRepository($conn);
$appleRepo = new AppleRepository($conn);

$gardensInfoQuery = $gardenRepo->getAllGardens();

$gardens = [];
foreach ($gardensInfoQuery as $garden) {
    $rottenApples  = 0;
    $fallenApples = 0;
    $applesInGarden = 0;

    $gardenInfo[$garden['id']] = $garden['name'];

    $treesInfo = $treeRepo->selectTreesByGardenId((int)$garden['id']);
    foreach ($treesInfo as $tree) {
        $apples = $appleRepo->selectApplesByTreeId($tree['id']);

        foreach ($apples as $apple) {
            if ( (int) ($apple['rottenness']) === Apple::ROTTENNESS_ROTTEN) {
                $rottenApples++;
            }
            if ( (int) ($apple['location']) === Apple::LOCATION_GROUND) {
                $fallenApples++;
            }
            $applesInGarden++;
        }
    }

    $gardens[] = [
        'gardenName' => $garden['name'],
        'gardenAge' => $garden['age'],
        'rottenApples' => $rottenApples,
        'fallenApples' => $fallenApples,
        'applesInGarden' => $applesInGarden,
        'treesInGarden' => count($treesInfo),
    ];
}

echo json_encode($gardens);