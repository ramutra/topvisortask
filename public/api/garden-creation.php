<?php

require_once "../../bootstrap.php";
ini_set('display_errors', 1);

use App\Database\Connection;

use App\Models\Garden;
use App\Models\Tree;
use App\Models\Apple;

use App\Repositories\GardenRepository;
use App\Repositories\TreeRepository;
use App\Repositories\AppleRepository;

if ($_SERVER['REQUEST_METHOD'] === 'POST') {
    $gardenName = $_POST['gardenName'];

    $randGardenAge = rand(1, 30);

    $garden = new Garden($gardenName, $randGardenAge);
    $conn = Connection::getConnection();

    try {
        $conn->beginTransaction();

        $gardenRepo = new GardenRepository($conn);
        $treeRepo = new TreeRepository($conn);
        $appleRepo = new AppleRepository($conn);


        if ($gardenRepo->isGardenExist($garden->getName())) {
            http_response_code(400);
            echo sprintf('Garden with such name: "%s"  already exists', $garden->getName());
        } else {
            createGarden($gardenRepo, $treeRepo, $appleRepo, $garden);
            echo sprintf('Garden "%s" successfully created', $garden->getName());
        }

        $conn->commit();
    } catch (Exception $e) {
        $conn->rollBack();


    }
}

/**
 * @param GardenRepository $gardenRepo
 * @param TreeRepository $treeRepo
 * @param AppleRepository $appleRepo
 * @param Garden $garden
 */
function createGarden(
    GardenRepository $gardenRepo,
    TreeRepository $treeRepo,
    AppleRepository $appleRepo,
    Garden $garden
): void {
    $gardenRepo->storeGarden($garden);
    $garden->setId($gardenRepo->getGardenId($garden->getName()));

    $randNumberOfTrees = rand(1, 99);

    $trees = [];
    for ($i = 0; $i < $randNumberOfTrees; $i++) {
        $treeAge = rand(0, $garden->getAge());
        $trees[] = new Tree($garden->getId(), $treeAge);
    }

    $trees = $treeRepo->storeTrees($trees);
    createApples($trees, $garden, $appleRepo);
}

/**
 * @param $trees Tree[]
 * @param Garden $garden
 * @param AppleRepository $appleRepo
 * @return Apple[]
 */
function createApples(array $trees, Garden $garden, AppleRepository $appleRepo): array
{
    $randNumberOfApples = rand(1, 99);

    $apples = [];
    foreach ($trees as $tree) {
        for ($i = 0; $i < $randNumberOfApples; $i++) {
            $appleAge = rand(0, $tree->getAge($garden->getAge()));

            $location = Apple::LOCATION_TREE;
            if ($appleAge === Apple::APPLE_FALL_FROM_TREE_ON_DAY) {
                $location = Apple::LOCATION_GROUND;
            }

            $apples[] = new Apple($tree->getId(), $appleAge, $location);
        }
    }

    return $appleRepo->storeApples($apples);
}
