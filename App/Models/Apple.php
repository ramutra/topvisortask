<?php

declare(strict_types=1);

namespace App\Models;

class Apple
{
    const LOCATION_TREE = 0;
    const LOCATION_GROUND = 1;

    const ROTTENNESS_FRESH = 0;
    const ROTTENNESS_ROTTEN = 1;

    const APPLE_FALL_FROM_TREE_ON_DAY = 30;
    const APPLE_BECOME_ROTTEN_ON_DAY = 31;

    private int $id;
    private int $gardenAgeOnAppleCreation;
    private int $location;
    private int $rottenness;
    private $treeId;

    public function __construct(
        int $treeId,
        int $gardenAgeOnAppleCreation,
        int $location = self::LOCATION_TREE,
        int $rottenness = self::ROTTENNESS_FRESH
    ) {
        $this->location = $location;
        $this->rottenness = $rottenness;
        $this->treeId = $treeId;
        $this->gardenAgeOnAppleCreation = $gardenAgeOnAppleCreation;
    }

    public function getLocation(): int
    {
        return $this->location;
    }
    public function setLocation(int $location)
    {
        $this->location = $location;
    }

    public function getAge(int $gardenAge): int
    {
        return $gardenAge - $this->gardenAgeOnAppleCreation;
    }

    public function getRottenness(): int
    {
        return $this->rottenness;
    }
    public function setRottenness(int $rottenness)
    {
        $this->rottenness = $rottenness;
    }

    public function getTreeId(): int
    {
        return $this->treeId;
    }
    public function setTreeId(int $treeId)
    {
        $this->treeId = $treeId;
    }

    /**
     * @return int
     */
    public function getGardenAgeOnAppleCreation(): int
    {
        return $this->gardenAgeOnAppleCreation;
    }

    /**
     * @param int $gardenAgeOnAppleCreation
     */
    public function setGardenAgeOnAppleCreation(int $gardenAgeOnAppleCreation): void
    {
        $this->gardenAgeOnAppleCreation = $gardenAgeOnAppleCreation;
    }

    /**
     * @return int
     */
    public function getId(): int
    {
        return $this->id;
    }

    /**
     * @param int $id
     */
    public function setId(int $id): void
    {
        $this->id = $id;
    }
}
