<?php

declare(strict_types=1);

namespace App\Models;

class Tree
{
    private int $id;
    private int $age;
    private int $gardenId;
    private int $gardenAgeOnTreeCreation;

    /**
     * Tree constructor.
     * @param int $gardenId
     * @param int $gardenAgeOnTreeCreation
     */
    public function __construct(int $gardenId, int $gardenAgeOnTreeCreation)
    {
        $this->gardenId = $gardenId;
        $this->gardenAgeOnTreeCreation = $gardenAgeOnTreeCreation;
    }

    /**
     * @return int
     */
    public function getAge(int $gardenAge): int
    {
        return $gardenAge - $this->gardenAgeOnTreeCreation;
    }

    /**
     * @return int
     */
    public function getGardenId(): int
    {
        return $this->gardenId;
    }

    /**
     * @param int $gardenId
     */
    public function setGardenId(int $gardenId): void
    {
        $this->gardenId = $gardenId;
    }

    /**
     * @return int
     */
    public function getGardenAgeOnTreeCreation(): int
    {
        return $this->gardenAgeOnTreeCreation;
    }

    /**
     * @param int $gardenAgeOnTreeCreation
     */
    public function setGardenAgeOnTreeCreation(int $gardenAgeOnTreeCreation): void
    {
        $this->gardenAgeOnTreeCreation = $gardenAgeOnTreeCreation;
    }

    /**
     * @return int
     */
    public function getId(): int
    {
        return $this->id;
    }

    /**
     * @param int $id
     */
    public function setId(int $id): void
    {
        $this->id = $id;
    }
}
