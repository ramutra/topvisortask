<?php

namespace App\Database;

require_once '../../config/db.php';

use PDO;

class Connection
{
    /**
     * @var PDO null
     */
    static private $conn = null;

    public static function getConnection(): PDO
    {
        if (self::$conn === null) {
            $dsn = sprintf("mysql:host=%s;dbname=%s", DB_HOST, DB_NAME);

            self::$conn = new PDO(
                $dsn,
                DB_USER,
                DB_PASSWORD,
                array(PDO::ATTR_ERRMODE => PDO::ERRMODE_EXCEPTION)
            );
        }

        return self::$conn;
    }
}