<?php

declare(strict_types=1);

function tableGarden($conn)
{
    $createTableGarden = 'CREATE TABLE
    IF
    NOT EXISTS gardens (
        id SMALLINT NOT NULL auto_increment,
        name VARCHAR (255) UNIQUE,
        age SMALLINT,
    PRIMARY KEY ( id ));';
    $gardenCreationQuery = $conn->query($createTableGarden) or die(print_r($conn->errorInfo()));

    return $gardenCreationQuery;
}

function tableAppleTrees($conn)
{
    $createTableAppleTrees = 'CREATE TABLE
    IF
    NOT EXISTS trees (
        id SMALLINT NOT NULL auto_increment,
        garden_id SMALLINT,
        garden_age_on_tree_creation SMALLINT,
    PRIMARY KEY ( id ),
    FOREIGN KEY (garden_id) REFERENCES gardens (id) 
    );';
    $treesCreationQuery = $conn->query($createTableAppleTrees) or die(print_r($conn->errorInfo()));

    return $treesCreationQuery;
}

function tableApples($conn)
{
    $createTableApples = 'CREATE TABLE
    IF
    NOT EXISTS apples (
        id SMALLINT NOT NULL auto_increment,
        location SMALLINT,
        rottenness SMALLINT,
        garden_age_on_apple_creation SMALLINT,
        tree_id SMALLINT,
    PRIMARY KEY ( id ),
    FOREIGN KEY (tree_id) REFERENCES trees (id) 
    );';
    $applesCreationQuery = $conn->query($createTableApples) or die(print_r($conn->errorInfo()));

    return $applesCreationQuery;
}
