<?php

namespace App\Repositories;

use PDO;
use App\Models\Garden;

class GardenRepository
{
    private PDO $conn;

    public function __construct(PDO $conn)
    {
        $this->conn = $conn;
    }

    public function getGardenId(string $name): int
    {
        $gardenId = $this->conn->prepare("SELECT id FROM gardens WHERE name = :name LIMIT 1");
        $gardenId->execute([':name' => $name]);

        return $gardenId->fetch(PDO::FETCH_ASSOC)["id"];
    }

    public function isGardenExist(string $name): bool
    {
        $gardenExistenceQuery = $this->conn->prepare("SELECT name FROM gardens WHERE name = :name");
        $gardenExistenceQuery->execute([':name' => $name]);

        if (!$gardenExistenceQuery->fetch(PDO::FETCH_ASSOC)) {
            return false;
        }

        return true;
    }

    public function storeGarden(Garden $garden): void
    {
        $gardenQuery = $this->conn->prepare("INSERT INTO gardens (name, age) VALUES (:name, :age);");
        $gardenQuery->execute([
            ':name' => $garden->getName(),
            ':age' => $garden->getAge(),
        ]);
    }

    public function changeGardenAge(string $name): void
    {
        $changeGardenAgeQuery = $this->conn->prepare("UPDATE gardens SET age = age + 1 WHERE name = :name");
        $changeGardenAgeQuery->execute([':name' => $name]);
    }

    public function selectGardenByName(string $name): array
    {
        $gardenInfoQuery = $this->conn->prepare("SELECT * FROM gardens WHERE name = :name");
        $gardenInfoQuery->execute([':name' => $name]);

        return $gardenInfoQuery->fetch(PDO::FETCH_ASSOC);
    }

    public function getAllGardens(): array
    {
        $allGardenInfoQuery = $this->conn->prepare("SELECT * FROM gardens");
        $allGardenInfoQuery->execute();

        $gardens = $allGardenInfoQuery->fetchAll(PDO::FETCH_ASSOC);

        if (!$gardens) {
            return [];
        }

        return $gardens;
    }
}
