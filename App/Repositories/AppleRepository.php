<?php

namespace App\Repositories;

use PDO;
use App\Models\Apple;

class AppleRepository
{
    private PDO $conn;

    public function __construct(PDO $conn)
    {
        $this->conn = $conn;
    }

    /**
     * @param $apples Apple[]
     * @return Apple[]
     */
    public function storeApples(array $apples): array
    {
        $treeQuery = $this->conn->prepare("
            INSERT INTO apples (tree_id, location, rottenness, garden_age_on_apple_creation)
            VALUES (:tree_id, :location, :rottenness, :garden_age_on_apple_creation);
        ");

        foreach ($apples as $apple) {
            $treeQuery->execute([
                ':garden_age_on_apple_creation' => $apple->getGardenAgeOnAppleCreation(),
                ':tree_id' => $apple->getTreeId(),
                ':location' => $apple->getLocation(),
                ':rottenness' => $apple->getRottenness(),
            ]);

            $result = $this->conn->query("SELECT LAST_INSERT_ID() as id", PDO::FETCH_ASSOC);
            $id = $result->fetch();
            $apple->setId((int)$id);
        }

        return $apples;
    }

    public function selectApplesByTreeId(int $id): array
    {
        $applesSelectionQuery = $this->conn->prepare("SELECT * FROM apples WHERE tree_id = :tree_id");
        $applesSelectionQuery->execute([':tree_id' => $id]);

        return $applesSelectionQuery->fetchAll(PDO::FETCH_ASSOC);
    }

    public function UpdateAppleLocation(int $ids, int $location = 1): void
    {
        $updateAppleLocation = $this->conn->prepare("UPDATE apples SET location = :location WHERE id = :id");
        $updateAppleLocation->execute([':id' => $ids, ':location' => $location]);
    }

    public function UpdateAppleRottenness(int $ids, int $rottenness = 1): void
    {
        $updateAppleRottenness = $this->conn->prepare(
            "UPDATE apples SET rottenness = :rottenness WHERE id = :id"
        );
        $updateAppleRottenness->execute([
            ':id' => $ids, ':rottenness' => $rottenness
        ]);
    }
}
