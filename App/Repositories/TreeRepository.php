<?php

namespace App\Repositories;

use PDO;
use App\Models\Tree;

class TreeRepository
{
    private PDO $conn;

    public function __construct(PDO $conn)
    {
        $this->conn = $conn;
    }

    /**
     * @param $trees Tree[]
     * @return Tree[]
     */
    public function storeTrees(array $trees): array
    {
        $treeQuery = $this->conn->prepare("
            INSERT INTO trees (garden_id, garden_age_on_tree_creation)
            VALUES (:garden_id, :garden_age_on_tree_creation);
        ");

        foreach ($trees as $tree) {
            $treeQuery->execute([
                ':garden_age_on_tree_creation' => $tree->getGardenAgeOnTreeCreation(),
                ':garden_id' => $tree->getGardenId(),
            ]);

            $result = $this->conn->query("SELECT LAST_INSERT_ID() as id", PDO::FETCH_ASSOC)->fetch();
            $treeId = (int)$result["id"];
            $tree->setId($treeId);
        }

        return $trees;
    }

    public function selectTreesByGardenId(int $id): array
    {
        $treesSelectionQuery = $this->conn->prepare("SELECT * FROM trees WHERE garden_id = :gardenId");
        $treesSelectionQuery->execute([':gardenId' => $id]);

        return $treesSelectionQuery->fetchAll(PDO::FETCH_ASSOC);
    }
}
